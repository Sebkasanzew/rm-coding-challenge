export const submit = reminder => ({
    type: 'SUBMIT_DATA',
    reminder
})
