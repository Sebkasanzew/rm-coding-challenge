import { combineReducers } from "redux";
import reminder from './reminder';

export default combineReducers({
    reminder
})
