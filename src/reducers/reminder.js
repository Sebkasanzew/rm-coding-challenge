const reminder = (state = [], action) => {
    switch (action.type) {
        case 'SUBMIT_DATA':
            return action.reminder
        default:
            return state
    }
}

export default reminder