import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import rootReducer from './reducers'

import "typeface-roboto";
import "./index.css";
import AddReminder from "./add-reminder/AddReminder";
import ReminderData from "./add-reminder/ReminderData";
import registerServiceWorker from "./registerServiceWorker";

const store = createStore(rootReducer)

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div>
        <Route path="/add-reminder" component={AddReminder}/>
        <Route path="/reminder" component={ReminderData}/>

        <Route exact path="/" component={AddReminder}/>
      </div>
    </Router>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
