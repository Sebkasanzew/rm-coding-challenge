import React, {Component} from "react";

import { connect } from 'react-redux';


class ReminderData extends Component {
    constructor(props) {
        super(props);

        this.state = {
            reminder: {}
        };
    }

    render() {
        const {json} = this.state;

        console.log(this.props)

        return ( 
            <ul>{ json }</ul>
        );
    }
}

const mapStateToProps = state => ({
    reminder: state.reminder
})

export default connect(
    mapStateToProps
)(ReminderData)
