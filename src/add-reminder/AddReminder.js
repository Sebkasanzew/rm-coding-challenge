import React, {
    Component
} from "react";
import {Link} from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import { connect } from 'react-redux';
import { submit } from "../actions";

/*const API =
    "https://api-gateway.remind.me/provider/ui/categoryProvider/CUSTOM/all";*/
const API =
    "./../static-api.json";

class AddReminder extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: "",
            category: "",
            provider: "",
            endOfContract: "",
            noticePeriod: "",
            categoryList: ["f", "sg", "w"],
            providerList: ["ya", "ne", "bob"],
            data: [],
            isLoading: false,
            error: null
        };
    }

    componentDidMount() {
        this.setState({
            isLoading: true
        });

        fetch(API)
            .then(response => response.json())
            .then(data => this.setState({
                data: "data.toArray()",
                isLoading: false
            }))
            .catch(error => this.setState({
                error,
                isLoading: false
            }));
    }

    
    handleChange = name => event => {
        console.log("name: " + name)

        this.setState({
          [name]: event.target.value,
        });
      };

    render() {
        const {json} = this.state;
        const { classes } = this.props;

        /*
        .map(hit => (
            <li key={hit.uid}>
                <p>{hit.title}</p>
            </li>
            ))
        */


        return (
            <div>
                <ul>{ json }</ul>

                <form name="reminder">
                <Grid container spacing={24}>

                <Grid item xs={12}>
                    <TextField
                    id="title"
                    label="title"
                    required
                    //className={classes.title}
                    value={this.state.title}
                    onChange={this.handleChange("title")}/>
                    </Grid>
                    
                    <Grid item xs={12}>
                    <FormControl required //className={classes.category}
                    >
                    <InputLabel htmlFor="category">category</InputLabel>
                    <Select
                        value={this.state.category}
                        onChange={this.handleChange("category")}
                        inputProps={{
                        name: 'category',
                        id: 'category',
                        }}
                    >
                        {this.state.categoryList.map((c, i) => {
                            return <MenuItem value={c} key={i}>{c}</MenuItem>
                        })}
                    </Select>
                    </FormControl>
                    </Grid>

                    <Grid item xs={12}>
                    <FormControl required //className={classes.provider}
                    >
                    <InputLabel htmlFor="provider">provider</InputLabel>
                    <Select
                        value={this.state.provider}
                        onChange={this.handleChange("provider")}
                        inputProps={{
                        name: 'provider',
                        id: 'provider',
                        }}
                    >
                        {this.state.providerList.map((p, i) => {
                            return <MenuItem value={p} key={i}>{p}</MenuItem>
                        })}
                    </Select>
                    </FormControl>
                    </Grid>
                    
                    <Grid item xs={12}>
                    <TextField
                    id="endOfContract"
                    label="end of contract"
                    required
                    type="date"
                    //className={classes.title}
                    value={this.state.endOfContract}
                    onChange={this.handleChange("endOfContract")}/>
                    </Grid>

                    <Grid item xs={12}>
                    <TextField
                    id="noticePeriod"
                    label="notice period"
                    type="date"
                    //className={classes.title}
                    value={this.state.noticePeriod}
                    onChange={this.handleChange("noticePeriod")}/>
                    </Grid>

                    <Grid item xs={12}>
                        <Button variant="contained" color="primary" type="submit" onClick={
                            (event) => {
                                event.preventDefault()
                                this.props.send(this.state)
                        }}>
                            <Link to="/reminder" type="submit">Next</Link>
                        </Button>
                    </Grid>

                    </Grid>
                </form>

               
            </div>
        );
    }
}

const mapStateToProps = state => ({
    reminder: state
})

const mapDispatchToProps = dispatch => ({
    send: (state) => dispatch(submit(state))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddReminder)
